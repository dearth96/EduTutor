import mysql.connector
from mysql.connector import errorcode
import sys
DB_NAME="stud1"
def create_database(cursor):
	try:
		cursor.execute(
			"CREATE DATABASE {} DEFAULT CHARACTER SET 'utf8'".format(DB_NAME))
	except mysql.connector.Error as err:
		print("Failed creating database: {}".format(err))
		exit(1)	
        

conf={'user':sys.argv[1], 'password':sys.argv[2],'host':'127.0.0.1'}
try:	
	cnx = mysql.connector.connect(**conf)
except mysql.connector.errors.ProgrammingError as err:
	if err.errno ==	errorcode.ER_ACCESS_DENIED_ERROR:
		print("Something is wrong with your user name or password")
	elif err.errno == errorcode.ER_BAD_DB_ERROR:
		print("Database does not exist")
	else:
		print(err)	
else:
	TABLES={}
	TABLES['tutor']=(
		"CREATE TABLE `tutor`("
		"	tut_id varchar(15) NOT NULL,"
		"	psswd varchar(20) NOT NULL,"
		"	tut_name varchar(20) NOT NULL)")
	TABLES['student']=(
		"CREATE TABLE `student`("
		"	stud_usn varchar(15) NOT NULL,"
		"	psswd varchar(20) NOT NULL,"
		"	stud_name varchar(20) NOT NULL)")
	cur=cnx.cursor()
	try:
		cnx.database = DB_NAME  
	except mysql.connector.Error as err:
		if err.errno == errorcode.ER_BAD_DB_ERROR:
			create_database(cur)
			cnx.database = DB_NAME
			for name, ddl in TABLES.items():
				try:
					print("Creating table {}: ".format(name), end='')
					cur.execute(ddl)
				except mysql.connector.Error as err:
					if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
						print("already exists.")
					else:
						print(err.msg)
				else:
					print("OK")
	cnx.close()